//
//  UserProfileViewController.swift
//  Wasup
//
//  Created by DomTom on 1/19/16.
//  Copyright © 2016 DomTom. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController
{

    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backButtonTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil);
    }
}
