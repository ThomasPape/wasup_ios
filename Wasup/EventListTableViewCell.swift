//
//  EventListTableViewCell.swift
//  Wasup
//
//  Created by DomTom on 2/2/16.
//  Copyright © 2016 DomTom. All rights reserved.
//

import UIKit

class EventListTableViewCell: UITableViewCell
{
    @IBOutlet weak var eventTitleLabel: UILabel!
    @IBOutlet weak var eventDateTimeLabel: UILabel!
    
    func loadEventData(_ eventTitle: String, eventDateTime: String)
    {
        self.eventTitleLabel.text = eventTitle;
        
        self.eventDateTimeLabel.text = eventDateTime;
    }
}
