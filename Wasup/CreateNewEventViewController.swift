//
//  CreateNewEventViewController.swift
//  Wasup
//
//  Created by DomTom on 12/7/15.
//  Copyright © 2015 DomTom. All rights reserved.
//

import UIKit

class CreateNewEventViewController: UIViewController, URLSessionDelegate, URLSessionTaskDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var eventTitleText: UITextField!
    @IBOutlet weak var eventDescriptionText: UITextView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // currently selected text field to handle keyboard positioning
    var activeField: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        self.eventTitleText.delegate = self;
        self.eventDescriptionText.delegate = self;
        
        NotificationCenter.default.addObserver(self, selector: #selector(CreateNewEventViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(CreateNewEventViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //self.performSegueWithIdentifier("loginView", sender: self);
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField .resignFirstResponder();
        
        return true;
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        textView .resignFirstResponder();
        
        return true;
    }
    
    func keyboardWillShow(_ notification: Notification)
    {
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let _ = activeField
        {
            if (!aRect.contains(activeField!.frame.origin))
            {
                self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification)
    {
        //Once keyboard disappears, restore original positions
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = false
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        activeField = textField;
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        activeField = textView;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        activeField = nil;
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        activeField = nil;
    }
    
    @IBAction func onCancelButtonPressed(_ sender: UIButton)
    {
        // go back to the homepage
        self.dismiss(animated: true, completion: nil);
    }
    
    func getFormattedDate() -> String
    {
        let utcDateFormatter = DateFormatter();
        utcDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss";
        utcDateFormatter.timeZone = TimeZone(abbreviation: "UTC");
        
        let dateString = utcDateFormatter.string(from: (datePicker?.date)!);
        
        return dateString;
    }
    
    @IBAction func newEventTapped(_ sender: UIButton)
    {
        //
        let userID = UserDefaults.standard.integer(forKey: "UserID");
        let eventDate = getFormattedDate();
        let eventTitle = eventTitleText?.text;
        let eventDescription = eventDescriptionText?.text;
        
        // Send user data to server
        let myUrl = URL(string: "http://ec2-54-68-92-237.us-west-2.compute.amazonaws.com/api/v1/UserEvents");
        var request = URLRequest(url:myUrl!);
        request.httpMethod = "POST";
        
        let postString = "user_id=\(userID)&event_date=\(eventDate)&event_title=\(eventTitle!)&event_description=\(eventDescription!)";
        
        request.httpBody = postString.data(using: String.Encoding.utf8);
        
        let config = URLSessionConfiguration.default;
        
        let session = URLSession(configuration: config, delegate: self, delegateQueue:OperationQueue.main);
        
        let task = session.dataTask(with: request, completionHandler: {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            do
            {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
            
                if let parseJSON = json {
                    print(parseJSON);
                }
            
            }
            catch let error as NSError {
                //Catch fires here, with an NSError being thrown from the JSONObjectWithData method
                print("A JSON parsing error occurred, here are the details:\n \(error)")
            }
            
            
            // go back to homepage
            self.dismiss(animated: true, completion: nil);

            
            
        }) 
        
        task.resume();
        
    }


}
