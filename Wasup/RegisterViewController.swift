//
//  RegisterViewController.swift
//  Wasup
//
//  Created by DomTom on 9/26/15.
//  Copyright © 2015 DomTom. All rights reserved.
//

import UIKit
import Foundation

class RegisterViewController: UIViewController, URLSessionDelegate, URLSessionTaskDelegate, UITextFieldDelegate {

    @IBOutlet weak var userFirstNameTextField: UITextField!
    @IBOutlet weak var userLastNameTextField: UITextField!
    @IBOutlet weak var userEmailTextField: UITextField!
    @IBOutlet weak var userUsernameTextField: UITextField!
    @IBOutlet weak var userPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // currently selected text field to handle keyboard positioning
    var activeField: UITextField?
    
    
    override func viewDidLoad() {
        super.viewDidLoad();
        // Do any additional setup after loading the view, typically from a nib.
        
        self.userFirstNameTextField.delegate = self;
        self.userLastNameTextField.delegate = self;
        self.userEmailTextField.delegate = self;
        self.userUsernameTextField.delegate = self;
        self.userPasswordTextField.delegate = self;
        self.confirmPasswordTextField.delegate = self;
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField .resignFirstResponder();
        
        return true;
    }
    
    func keyboardWillShow(_ notification: Notification)
    {
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let _ = activeField
        {
            if (!aRect.contains(activeField!.frame.origin))
            {
                self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
            }
        }

    }
    
    func keyboardWillHide(_ notification: Notification)
    {
        //Once keyboard disappears, restore original positions
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = false

    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        activeField = textField;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        activeField = nil;
    }
    
    func displayAlertMessage(_ userMessage:String)
    {
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil);
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil);
    }
    
    
    @IBAction func registerButtonTapped(_ sender: UIButton)
    {
        let userFirstName = userFirstNameTextField?.text;
        let userLastName = userLastNameTextField?.text;
        let username = userUsernameTextField?.text;
        let userEmail = userEmailTextField?.text;
        let userPassword = userPasswordTextField?.text;
        let confirmPassword = confirmPasswordTextField?.text;
        
        // Check for emptry fields
        if(userFirstName!.isEmpty || userLastName!.isEmpty || username!.isEmpty || userEmail!.isEmpty || userPassword!.isEmpty || confirmPassword!.isEmpty)
        {
            displayAlertMessage("All fields are required.");
            
            return;
        }
        
        // Check if password fields match up
        if(userPassword != confirmPassword)
        {
            displayAlertMessage("The passwords entered do not match.");
            
            return;
        }
        
        // Send user data to server 
        let myUrl = URL(string: "http://ec2-54-68-92-237.us-west-2.compute.amazonaws.com/api/v1/UserRegister");
        var request = URLRequest(url:myUrl!);
        request.httpMethod = "POST";
        
        let postString = "first_name=\(userFirstName!)&last_name=\(userLastName!)&username=\(username!)&email=\(userEmail!)&password=\(userPassword!)";
        
        request.httpBody = postString.data(using: String.Encoding.utf8);
        
        let config = URLSessionConfiguration.default;
        
        let session = Foundation.URLSession(configuration: config, delegate: self, delegateQueue:OperationQueue.main);
        
        let task = session.dataTask(with: request, completionHandler: {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)");
                return;
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                
                if let parseJSON = json {
                    let resultValue = parseJSON["status"] as? String;
                    print(json);
                    
                    var isUserRegistered:Bool = false;
                    if(resultValue == "Success")
                    {
                        isUserRegistered = true;
                    }
                    
                    var messageToDisplay:String = parseJSON["message"] as! String!;
                    if(!isUserRegistered)
                    {
                        messageToDisplay = parseJSON["message"] as! String!;
                    }
                    
                    DispatchQueue.main.async(execute: {
                        
                        // TODO: this is bad I know it cant be the right thing to do here 
                        UserDefaults.standard.set(username, forKey:"username");
                        UserDefaults.standard.set(userPassword, forKey:"userPassword");
                        UserDefaults.standard.synchronize();
                        
                        // Display alert message with confirmation
                        let myAlert = UIAlertController(title:"Alert", message:messageToDisplay, preferredStyle: UIAlertControllerStyle.alert);
                        
                        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ action in
                            self.dismiss(animated: true, completion: nil);
                        }
                        
                        myAlert.addAction(okAction);
                        self.present(myAlert, animated: true, completion: nil);
                    });
                }

            }
            catch let error as NSError {
                // Catch fires here, with an NSError being thrown from the JSONObjectWithData method
                print("A JSON parsing error occurred, here are the details:\n \(error)");
            }
            
            
        }) 
        
        task.resume();
        
    }
    
    @IBAction func signInButtonTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil);
    }
    

    func urlSession(_ session: URLSession,
                    task: URLSessionTask,
                    didReceive challenge: URLAuthenticationChallenge,
                    completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void)
    {
        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust)
        {
            if (challenge.protectionSpace.host == "ec2-54-68-92-237.us-west-2.compute.amazonaws.com")
            {
                let credentials = URLCredential(trust: challenge.protectionSpace.serverTrust!);
                challenge.sender!.use(credentials, for: challenge);
            }
        }
        
        challenge.sender!.continueWithoutCredential(for: challenge);
    }
}

