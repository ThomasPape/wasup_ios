//
//  UserHomepageViewController.swift
//  Wasup
//
//  Created by DomTom on 9/26/15.
//  Copyright © 2015 DomTom. All rights reserved.
//

import UIKit

class UserHomepageViewController: UIViewController, URLSessionDelegate, URLSessionTaskDelegate, UITableViewDelegate, UITableViewDataSource
{
    
    @IBOutlet var tableView: UITableView!
    
    var userEventsTable = EventHashTable(capacity: 100);
    var calanderDateData = [Int]();
    
    let eventListHeaderHeight: CGFloat = 35.0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // register the friendsListTableCell
        let nib = UINib(nibName: "EventListTableViewCell", bundle: nil);
        self.tableView.register(nib, forCellReuseIdentifier: "eventListCell");
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(UserHomepageViewController.handleSwipes(_:)));
        swipeRight.direction = UISwipeGestureRecognizerDirection.right;
        self.view.addGestureRecognizer(swipeRight);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        // check to see if we are logged in already
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn");
        
        
        // if not logged in show login view
        if(!isUserLoggedIn)
        {
            self.performSegue(withIdentifier: "loginView", sender: self);
        }
        else
        {
            fillUserEventsList();
        }
    }
    
    func handleSwipes(_ sender: UISwipeGestureRecognizer)
    {
        if(sender.direction == .right)
        {
            self.performSegue(withIdentifier: "", sender: nil);
        }
    }
    
    // Event List Starts Here!
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.userEventsTable.count();
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return eventListHeaderHeight;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let dateHash = self.calanderDateData[section];
        
        var eventNode = self.userEventsTable.getNodeForHash(dateHash);
        
        var sectionRowCount = 1;
        
        while(eventNode?.next != nil)
        {
            eventNode = eventNode?.next;
            sectionRowCount += 1;
        }

        return sectionRowCount;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let sectionHeaderView = UIView(frame: CGRect(x: 0,y: 0,width: tableView.frame.size.width, height: eventListHeaderHeight));
        
        sectionHeaderView.backgroundColor = UIColor.lightGray;
        
        let headerLabel = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: sectionHeaderView.frame.width, height: sectionHeaderView.frame.height));
        headerLabel.textAlignment = NSTextAlignment.center;
        
        let dateIndex = self.calanderDateData[section];
        let dateNode = self.userEventsTable.getNodeForHash(dateIndex);
        
        let df = DateFormatter();
        df.dateFormat = "MM-dd";
        df.dateStyle = DateFormatter.Style.full;
        
        let eventDateStr = df.string(from: (dateNode?.eventDate)!);
        headerLabel.text = eventDateStr;
        
        sectionHeaderView.addSubview(headerLabel);
        
        return sectionHeaderView;
    }
    
    func getNSDateFromEventDate(_ eventDate: String) -> Date
    {
        let utcDateFormatter = DateFormatter();
        utcDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss";
        utcDateFormatter.timeZone = TimeZone(abbreviation: "UTC");
        
        let date = utcDateFormatter.date(from: eventDate);
        
        return date!;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:EventListTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "eventListCell") as! EventListTableViewCell;
        
        cell.selectionStyle = .none;
        
        
        let nodeIndex = self.calanderDateData[(indexPath as NSIndexPath).section];
        var eventNode = self.userEventsTable.getNodeForHash(nodeIndex);
        
        var index = 0;
        while(index < (indexPath as NSIndexPath).row && eventNode?.next != nil)
        {
            eventNode = eventNode?.next;
            index += 1;
        }
        
        cell.loadEventData((eventNode?.eventEntry.eventTitle)!, eventDateTime: (eventNode?.eventEntry.eventTime)!);
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true;
    }
    
    func deleteUserEvent(_ eventIndex: Int)
    {
        // fetch user ID from user data
//        let userID = UserDefaults.standard.integer(forKey: "UserID");
//        
//        let eventID = self.userEventsTable[eventIndex]["event_id"] as? String;
//        
//        
//        // Send user data to server
//        let myUrl = URL(string: "http://ec2-54-68-92-237.us-west-2.compute.amazonaws.com/api/v1/UserEvents");
//        var request = URLRequest(url:myUrl!);
//        request.httpMethod = "DELETE";
//        
//        let postString = "user_id=\(userID)&event_id=" + eventID;
//        
//        request.HTTPBody = postString.dataUsingEncoding(String.Encoding.utf8);
//        
//        let config = URLSessionConfiguration.default;
//        
//        let session = URLSession(configuration: config, delegate: self, delegateQueue:OperationQueue.main);
//        
//        let task = session.dataTask(with: request, completionHandler: {
//            data, response, error in
//            
//            if error != nil
//            {
//                print("error=\(error)");
//                return;
//            }
//            
//            do
//            {
//                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
//                
//                if let parseJSON = json {
//                    //let resultValue = parseJSON["status"] as? String
//                    //let resultMSG = parseJSON["message"] as? String
//                    
//                    print(parseJSON);
//                    
//                    self.fillUserEventsList();
//                }
//                
//            }
//            catch let error as NSError
//            {
//                // Catch fires here, with an NSError being thrown from the JSONObjectWithData method
//                print("A JSON parsing error occurred, here are the details:\n \(error)")
//            }
//        })
//        task.resume();
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if(editingStyle == UITableViewCellEditingStyle.delete)
        {
            deleteUserEvent((indexPath as NSIndexPath).row);
        }
    }
        
    @IBAction func logoutButtonTapped(_ sender: UIButton)
    {
        // clear isUserLoggedIn NSUserDefaults
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn");
        UserDefaults.standard.synchronize();
        
        // clear User Home Page data
        userEventsTable.clear();
        calanderDateData.removeAll();
        self.tableView.reloadData();
        
        UserData.sharedInstance.resetAllUserData();
        
        // activate the login view
        self.performSegue(withIdentifier: "loginView", sender: nil);
    }
    
    func fillUserEventsList()
    {
        // fetch user ID from user data
        let userID = UserDefaults.standard.integer(forKey: "UserID");
        
        print("user id is \(userID)");
        
        // Send user data to server
        let myUrl = URL(string: "http://ec2-54-68-92-237.us-west-2.compute.amazonaws.com/api/v1/UserEvents/user/\(userID)");
        var request = URLRequest(url:myUrl!);
        request.httpMethod = "GET";
        
        let config = URLSessionConfiguration.default;
        
        let session = URLSession(configuration: config, delegate: self, delegateQueue:OperationQueue.main);
        
        let task = session.dataTask(with: request, completionHandler: {
            data, response, error in
            
            if error != nil
            {
                print("URL Session error=\(error)");
                return;
            }
            
            do
            {
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary;
                
                if let parseJSON = json
                {
                    let status = parseJSON["status"] as? String;
                    
                    if(status == "success")
                    {
                        self.userEventsTable.clear();
                        self.calanderDateData.removeAll();
                        
                        if let eventData = parseJSON["event_data"] as? [Any]
                        {
                            for event in eventData
                            {
                                if let currEvent = event as? [String: Any]
                                {
                                
                                    let eventTitleStr = currEvent["event_title"] as? String;
                            
                                    let eventDate = currEvent["event_date"] as? String;
                                    let date = self.getNSDateFromEventDate(eventDate!);
                            
                                    var df = DateFormatter();
                                    df.dateFormat = "HH:mm:ss";
                                    df.timeStyle = DateFormatter.Style.short;
                                    let eventTimeStr = df.string(from: date);
                                
                                    df = DateFormatter();
                                    df.dateFormat = "yyyy-MM-dd";
                                    df.dateStyle = DateFormatter.Style.short;
                                    let eventDateStr = df.string(from: date);
                                    
                                    let newCalendarEntry = EventCalanderEntry(_eventTime: eventTimeStr, _eventTitle: eventTitleStr!);
                                
                                    let newHashValue = self.userEventsTable.addEvent(date, dateStr: eventDateStr, entry: newCalendarEntry);
                                
                                
                                    if (newHashValue != -1)
                                    {
                                        self.calanderDateData.append( newHashValue! );
                                    }
                                }
                            }
                            // sort the calander Date data in ascending order
                            self.calanderDateData.sort(by: {
                                let eventNode0 = self.userEventsTable.getNodeForHash($0);
                                let eventNode1 = self.userEventsTable.getNodeForHash($1);
                                return eventNode0!.eventDate.timeIntervalSince1970 < eventNode1!.eventDate.timeIntervalSince1970;
                            });
                            
                            print(self.calanderDateData);
                        }
                    }
                    else if(status == "error")
                    {
                        self.userEventsTable.clear();
                        self.calanderDateData.removeAll();
                        
                        let errorMessage = parseJSON["message"];
                        print(errorMessage);
                    }
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    self.tableView.reloadData();
                })
            }
            catch let error as NSError {
                // Catch fires here, with an NSError being thrown from the JSONObjectWithData method
                print("A JSON parsing error occurred, here are the details:\n \(error)")
            }
            
            
        })
        
        task.resume();

    }

    @IBAction func profileButtonTapped(_ sender: UIButton)
    {
        // activate the user profile view
        self.performSegue(withIdentifier: "ShowUserProfile", sender: nil);
    }
}
