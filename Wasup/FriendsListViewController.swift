//
//  FriendsListViewController.swift
//  Wasup
//
//  Created by DomTom on 1/11/16.
//  Copyright © 2016 DomTom. All rights reserved.
//

import UIKit

class FriendsListTableViewCell: UITableViewCell
{
    @IBOutlet weak var userIconImageView: UIImageView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    func loadItem(_ userName: String)
    {
        self.userNameLabel.text = userName;
    }
}

class FriendsListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{

    @IBOutlet weak var tableView: UITableView!
    
    var selectedUserID = 0;
    var selectedUsername = "";
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl();
        refreshControl.addTarget(self, action: #selector(FriendsListViewController.handleRefresh(_:)), for: .valueChanged);
        
        return refreshControl;
    }();
    
    override func viewDidLoad()
    {
        super.viewDidLoad();
        
        self.tableView.addSubview(self.refreshControl);
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell");
        self.tableView.rowHeight = 50.0;
        
        // register the friendsListTableCell
        let nib = UINib(nibName: "FriendsListTableViewCell", bundle: nil);
        self.tableView.register(nib, forCellReuseIdentifier: "friendsListCell");
        
        UserData.sharedInstance.fetchAcceptedFriendData();
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl)
    {
        UserData.sharedInstance.fetchAcceptedFriendData();
        
        self.tableView.reloadData();
        
        refreshControl.endRefreshing();
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let acceptedFriends = UserData.sharedInstance.getAcceptedFriendData();
        return acceptedFriends.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:FriendsListTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "friendsListCell") as! FriendsListTableViewCell;
        
        cell.selectionStyle = .none;
        
        let userFriends = UserData.sharedInstance.getAcceptedFriendData() as Array;
        
        if let userFN = userFriends[(indexPath as NSIndexPath).row]["username"] as? String
        {
            cell.loadItem(userFN);
        }
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let userFriends = UserData.sharedInstance.getAcceptedFriendData() as Array;
        
        if let userid = userFriends[(indexPath as NSIndexPath).row]["user_id"] as? String{
            selectedUserID = Int(userid)!;
        }
        if let username = userFriends[(indexPath as NSIndexPath).row]["username"] as? String{
            selectedUsername = username;
        }
        
        self.performSegue(withIdentifier: "ShowFriendCalander", sender: nil);
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "ShowFriendCalander")
        {
            let vc = segue.destination as! FriendCalanderListViewController;
            
            vc.userID = selectedUserID;
            vc.userNameStr = selectedUsername;
        }
    }
}
