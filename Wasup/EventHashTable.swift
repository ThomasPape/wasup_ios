//
//  EventHashTable.swift
//  Wasup
//
//  Created by DomTom on 2/7/16.
//  Copyright © 2016 DomTom. All rights reserved.
//

import Foundation

struct EventCalanderEntry
{
    var eventTime: String!;
    var eventTitle: String!;
    
    init(_eventTime: String, _eventTitle:String)
    {
        eventTime = _eventTime;
        eventTitle = _eventTitle;
    }
}

class HashNode
{
    var eventDate: Date!
    var eventDateString: String!
    var eventEntry: EventCalanderEntry!
    var next: HashNode!
}

class EventHashTable
{
    fileprivate var buckets: Array<HashNode?>;
    
    fileprivate var numberOfFilledBuckets: Int;
    
    subscript(index: Int) -> HashNode!
    {
        get
        {
            return self.buckets[index];
        }
        set(newValue)
        {
            self.buckets[index]! = newValue;
        }
    }
    
    subscript(key: String) -> HashNode!
    {
        get
        {
            let hashIndex = self.createHash(key);
            
            return self.buckets[hashIndex!];
        }
        set(newValue)
        {
            let hashIndex = self.createHash(key);
            
            self.buckets[hashIndex!] = newValue;
        }
    }
    
    // initialize the buckets with nil
    init(capacity: Int)
    {
        self.buckets = Array<HashNode!>(repeating: nil, count: capacity);
        self.numberOfFilledBuckets = 0;
    }
    
    func count() -> Int!
    {
        return numberOfFilledBuckets;
    }
    
    func getNodeForHash(_ hash:Int) -> HashNode!
    {
        return self.buckets[hash];
    }
    
    func getNodeForKey(_ key: String) -> HashNode!
    {
         let hashIndex = self.createHash(key);
        
        return self.buckets[hashIndex!];
    }
    
    func clear()
    {
        let size = self.buckets.count;
        
        self.buckets = Array<HashNode!>(repeating: nil, count: size);
        self.numberOfFilledBuckets = 0;
    }
    
    func addEvent(_ date: Date, dateStr: String, entry: EventCalanderEntry) -> Int!
    {
        var hashIndex: Int!
        
        hashIndex = self.createHash(dateStr);
        
        let childToUse: HashNode = HashNode();
        var head: HashNode!
        
        childToUse.eventDate = date;
        childToUse.eventDateString = dateStr;
        childToUse.eventEntry = entry;
        
        if(buckets[hashIndex] == nil)
        {
            buckets[hashIndex] = childToUse;
            numberOfFilledBuckets += 1;
            
            return hashIndex;
        }
        else
        {
            // A collision occured so we want to implement chaining
            // Since we are creating a calander we want to sort by times here.
            
            head = buckets[hashIndex];
            
            var currNode = head;
            var prevNode:HashNode? = nil;
            
            while( (currNode) != nil )
            {
                if( (currNode?.eventDate.timeIntervalSince1970)! > date.timeIntervalSince1970 )
                {
                    childToUse.next = currNode;
                    if let prev = prevNode
                    {
                        prev.next = childToUse;
                    }
                    else
                    {
                        // were setting the head
                        buckets[hashIndex] = head;
                    }
                    break;
                }
                else if( currNode?.next == nil )
                {
                    currNode?.next = childToUse;
                    break;
                }
                else
                {
                    prevNode = currNode;
                    currNode = currNode?.next;
                }
            }
        }
        
        return -1;
    }
    
    func createHash(_ date: String) -> Int!
    {
        var remainder: Int = 0;
        var divisor: Int = 0;
        
        // obtain the ascii value of each character
        for key in date.unicodeScalars
        {
            divisor += Int(key.value);
        }
        
        remainder = divisor % self.buckets.count;
        
        return remainder;
    }
}
