//
//  UserData.swift
//  Wasup
//
//  Created by DomTom on 1/31/16.
//  Copyright © 2016 DomTom. All rights reserved.
//

import Foundation

class UserData : NSObject, URLSessionDelegate, URLSessionTaskDelegate
{
    static let sharedInstance = UserData();
    
    var userAcceptedFriendsData = NSArray();
    
    var allUserFriendData = NSArray();
    
    func getAcceptedFriendData() -> NSArray { return userAcceptedFriendsData; }
    
    func getAllUserFriendData() -> NSArray { return allUserFriendData; }
    
    fileprivate override init()
    {
        super.init();
        
        fetchAcceptedFriendData();
        
        fetchAllUserFriendData();
    }
    
    func resetAllUserData()
    {
        userAcceptedFriendsData = [];
        allUserFriendData = [];
    }
    
    func fetchAllUserFriendData()
    {
        // fetch user ID from user data
        let userID = UserDefaults.standard.integer(forKey: "UserID");
        
        // set the URL
        let Url = URL(string: "http://ec2-54-68-92-237.us-west-2.compute.amazonaws.com/api/v1/UserFriends/all/\(userID)");
        var request = URLRequest(url:Url!);
        
        // Setup the Request
        request.httpMethod = "GET";
        
        let config = URLSessionConfiguration.default;
        
        let session = URLSession(configuration: config, delegate: self, delegateQueue:OperationQueue.main);
        
        let task = session.dataTask(with: request, completionHandler: {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            do
            {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                
                if let parseJSON = json
                {
                    let status = parseJSON["status"] as? String;
                    
                    if(status == "success")
                    {
                        if let UFData = parseJSON["user_friends"] as? NSArray
                        {
                            self.allUserFriendData = UFData;
                        }
                    }
                    else if(status == "error")
                    {
                        self.allUserFriendData = [];
                        
                        let errorMessage = parseJSON["message"];
                        print(errorMessage);
                    }
                }
            }
            catch let error as NSError {
                //Catch fires here, with an NSError being thrown from the JSONObjectWithData method
                print("A JSON parsing error occurred, here are the details:\n \(error)")
            }
        }) 
        
        task.resume();
    }

    
    func fetchAcceptedFriendData()
    {
        // fetch user ID from user data
        let userID = UserDefaults.standard.integer(forKey: "UserID");
        
        // set the URL
        let Url = URL(string: "http://ec2-54-68-92-237.us-west-2.compute.amazonaws.com/api/v1/UserFriends/accepted/\(userID)");
        var request = URLRequest(url:Url!);
        
        // Setup the Request
        request.httpMethod = "GET";
        
        let config = URLSessionConfiguration.default;
        
        let session = URLSession(configuration: config, delegate: self, delegateQueue:OperationQueue.main);
        
        let task = session.dataTask(with: request, completionHandler: {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            do
            {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                
                if let parseJSON = json
                {
                    let status = parseJSON["status"] as? String;
                    
                    if(status == "success")
                    {
                        if let UFData = parseJSON["user_friends"] as? NSArray
                        {
                            self.userAcceptedFriendsData = UFData;
                        }
                    }
                    else if(status == "error")
                    {
                        self.userAcceptedFriendsData = [];
                        
                        let errorMessage = parseJSON["message"];
                        print(errorMessage);
                    }

                }
            }
            catch let error as NSError {
                //Catch fires here, with an NSError being thrown from the JSONObjectWithData method
                print("A JSON parsing error occurred, here are the details:\n \(error)")
            }
        }) 
        
        task.resume();
    }
    
    func sendFriendStatusChange(_ friendID: Int)
    {
        // fetch user ID from user data
        let userID = UserDefaults.standard.integer(forKey: "UserID");
        
        let Url = URL(string: "http://ec2-54-68-92-237.us-west-2.compute.amazonaws.com/api/v1/UserFriends");
        var request = URLRequest(url:Url!);
        let postString = "user_id=\(userID)&friend_id=\(friendID)";
        
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        
        let config = URLSessionConfiguration.default;
        
        let session = URLSession(configuration: config, delegate: self, delegateQueue:OperationQueue.main);
        
        let task = session.dataTask(with: request, completionHandler: {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            do
            {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                
                if let parseJSON = json {
                    print(parseJSON);
                }
                
            }
            catch let error as NSError {
                //Catch fires here, with an NSError being thrown from the JSONObjectWithData method
                print("A JSON parsing error occurred, here are the details:\n \(error)")
            }
            
            DispatchQueue.main.async(execute: {
                self.fetchAllUserFriendData();
            });

        }) 
        
        task.resume();
    }

}
