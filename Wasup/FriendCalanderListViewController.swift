//
//  FriendCalanderListViewController.swift
//  Wasup
//
//  Created by DomTom on 2/28/16.
//  Copyright © 2016 DomTom. All rights reserved.
//

import UIKit

class FriendCalanderListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, URLSessionDelegate, URLSessionTaskDelegate {
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    var userEventsTable = EventHashTable(capacity: 100);
    var calanderDateData = [Int]();
    
    var userID: Int = 0 {
        didSet{
            fillUserEventsList()
        }
    }
    
    var userNameStr: String = "";
    
    let eventListHeaderHeight: CGFloat = 35.0;

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.tableView.delegate = self;
        self.tableView.dataSource = self;

        // register the friendsListTableCell
        let nib = UINib(nibName: "EventListTableViewCell", bundle: nil);
        self.tableView.register(nib, forCellReuseIdentifier: "eventListCell");
        
        usernameLabel.text = userNameStr;
        
        fillUserEventsList();
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Event List Starts Here!
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.userEventsTable.count();
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return self.eventListHeaderHeight;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let dateHash = self.calanderDateData[section];
        
        var eventNode = self.userEventsTable.getNodeForHash(dateHash);
        
        var sectionRowCount = 1;
        
        while(eventNode?.next != nil)
        {
            eventNode = eventNode?.next;
            sectionRowCount += 1;
        }
        
        return sectionRowCount;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let sectionHeaderView = UIView(frame: CGRect(x: 0,y: 0,width: tableView.frame.size.width, height: eventListHeaderHeight));
        
        sectionHeaderView.backgroundColor = UIColor.lightGray;
        
        let headerLabel = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: sectionHeaderView.frame.width, height: sectionHeaderView.frame.height));
        headerLabel.textAlignment = NSTextAlignment.center;
        
        let dateIndex = self.calanderDateData[section];
        let dateNode = self.userEventsTable.getNodeForHash(dateIndex);
        
        let df = DateFormatter();
        df.dateFormat = "MM-dd";
        df.dateStyle = DateFormatter.Style.full;
        
        let eventDateStr = df.string(from: (dateNode?.eventDate)!);
        headerLabel.text = eventDateStr;
        
        sectionHeaderView.addSubview(headerLabel);
        
        return sectionHeaderView;
    }
    
    func getNSDateFromEventDate(_ eventDate: String) -> Date
    {
        let utcDateFormatter = DateFormatter();
        utcDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss";
        utcDateFormatter.timeZone = TimeZone(abbreviation: "UTC");
        
        let date = utcDateFormatter.date(from: eventDate);
        
        return date!;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:EventListTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "eventListCell") as! EventListTableViewCell;
        
        cell.selectionStyle = .none;
        
        
        let nodeIndex = self.calanderDateData[(indexPath as NSIndexPath).section];
        var eventNode = self.userEventsTable.getNodeForHash(nodeIndex);
        
        var index = 0;
        while(index < (indexPath as NSIndexPath).row && eventNode?.next != nil)
        {
            eventNode = eventNode?.next;
            index += 1;
        }
        
        cell.loadEventData((eventNode?.eventEntry.eventTitle)!, eventDateTime: (eventNode?.eventEntry.eventTime)!);
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true;
    }
    
    func fillUserEventsList()
    {
        // Send user data to server
        let myUrl = URL(string: "http://ec2-54-68-92-237.us-west-2.compute.amazonaws.com/api/v1/UserEvents/user/\(userID)");
        var request = URLRequest(url:myUrl!);
        request.httpMethod = "GET";
        
        let config = URLSessionConfiguration.default;
        
        let session = URLSession(configuration: config, delegate: self, delegateQueue:OperationQueue.main);
        
        let task = session.dataTask(with: request, completionHandler: {
            data, response, error in
            
            if error != nil
            {
                print("URL Session error=\(error)");
                return;
            }
            
            do
            {
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers);
                
                if let parseJSON = json as? [String: Any]
                {
                    let status = parseJSON["status"] as? String;
                    
                    if(status == "success")
                    {
                        self.userEventsTable.clear();
                        self.calanderDateData.removeAll();
                        
                        if let eventData = parseJSON["event_data"] as? [Any]
                        {
                            for event in eventData
                            {
                                if let currEvent = event as? [String: Any]
                                {
                                
                                    let eventTitleStr = currEvent["event_title"] as? String;
                                
                                    let eventDate = currEvent["event_date"] as? String;
                                
                                    let date = self.getNSDateFromEventDate(eventDate!);
                                    
                                    var df = DateFormatter();
                                    df.dateFormat = "HH:mm:ss";
                                    df.timeStyle = DateFormatter.Style.short;
                                    let eventTimeStr = df.string(from: date);
                                    
                                    df = DateFormatter();
                                    df.dateFormat = "yyyy-MM-dd";
                                    df.dateStyle = DateFormatter.Style.short;
                                    let eventDateStr = df.string(from: date);
                                    
                                    let newCalendarEntry = EventCalanderEntry(_eventTime: eventTimeStr, _eventTitle: eventTitleStr!);
                                    
                                    let newHashValue = self.userEventsTable.addEvent(date, dateStr: eventDateStr, entry: newCalendarEntry);
                                    
                                    
                                    if (newHashValue != -1)
                                    {
                                        self.calanderDateData.append( newHashValue! );
                                    }

                                }
                                
                            }
                            // sort the calander Date data in ascending order
                            self.calanderDateData.sort(by: {
                                let eventNode0 = self.userEventsTable.getNodeForHash($0);
                                let eventNode1 = self.userEventsTable.getNodeForHash($1);
                                return eventNode0!.eventDate.timeIntervalSince1970 < eventNode1!.eventDate.timeIntervalSince1970;
                            });
                            
                            print(self.calanderDateData);
                        }
                    }
                    else if(status == "error")
                    {
                        self.userEventsTable.clear();
                        self.calanderDateData.removeAll();
                        
                        let errorMessage = parseJSON["message"];
                        print(errorMessage);
                    }
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    self.tableView.reloadData();
                })
            }
            catch let error as NSError {
                // Catch fires here, with an NSError being thrown from the JSONObjectWithData method
                print("A JSON parsing error occurred, here are the details:\n \(error)")
            }
            
            
        })
        
        task.resume();
        
    }

    @IBAction func onBackButtonTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil);
    }
}
