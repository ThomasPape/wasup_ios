//
//  AddFriendsViewController.swift
//  Wasup
//
//  Created by DomTom on 1/22/16.
//  Copyright © 2016 DomTom. All rights reserved.
//

import UIKit

class AddFriendsTableViewCell : UITableViewCell
{
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var followFriendButton: UIButton!
    
    enum FriendButtonStatus
    {
        case follow
        case requested
        case following
    }
    
    fileprivate var friendButtonStatus: FriendButtonStatus;
    
    fileprivate var friendID: Int;
    
    required init?(coder aDecoder: NSCoder)
    {
        friendButtonStatus = .follow;
        
        friendID = 0;
        
        super.init(coder: aDecoder)!;
    }
    
    func loadItem(_ username: String, friendStatus: FriendButtonStatus, friendUserID: Int)
    {
        userNameLabel.text = username;
        
        friendButtonStatus = friendStatus;
        
        friendID = friendUserID;
        
        updateFriendButton();
    }
    
    fileprivate func updateFriendButton()
    {
        var buttonTitleText = "";
        var buttonColor = self.followFriendButton.backgroundColor;
    
        switch self.friendButtonStatus
        {
        case .follow:
            buttonTitleText = "Follow";
            buttonColor = UIColor(red: 1.0, green: 128.0/255.0, blue: 0.0, alpha: 1.0);
        case .requested:
            buttonTitleText = "Requested";
            buttonColor = UIColor(red: 1.0, green: 0.5, blue: 1.0, alpha: 1.0);
        case .following:
            buttonTitleText = "Following";
            buttonColor = UIColor(red: 0.0, green: 1.0, blue: 0.0, alpha: 1.0);
        }
    
        self.followFriendButton.setTitle(buttonTitleText, for: UIControlState());
        self.followFriendButton.backgroundColor = buttonColor;
    }
    
    @IBAction func followFriendButtonTapped(_ sender: UIButton)
    {
        switch self.friendButtonStatus
        {
        case .follow:
            self.friendButtonStatus = .requested;
        case .requested:
            self.friendButtonStatus = .follow;
        case .following:
            self.friendButtonStatus = .follow;
        }
        
        updateFriendButton();
        
        UserData.sharedInstance.sendFriendStatusChange(friendID);
    }
}

class AddFriendsViewController: UIViewController, URLSessionDelegate, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var tableView: UITableView!
    
    var addFriends = [Any]();

    override func viewDidLoad()
    {
        super.viewDidLoad();

        // Do any additional setup after loading the view.
        self.tableView!.delegate = self;
        self.tableView!.dataSource = self;
        self.tableView!.rowHeight = 50.0;
        
        // register the addFriendsTableCell
        let nib = UINib(nibName: "AddFriendsTableViewCell", bundle: nil);
        self.tableView!.register(nib, forCellReuseIdentifier: "addFriendsCell");
        
        UserData.sharedInstance.fetchAllUserFriendData();
        
        fillAddFriendsList();
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        fillAddFriendsList();
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }
    
    func fillAddFriendsList()
    {
        // fetch user ID from user data
        let userID = UserDefaults.standard.integer(forKey: "UserID");
        
        // Get all user data except current user
        let myUrl = URL(string: "http://ec2-54-68-92-237.us-west-2.compute.amazonaws.com/api/v1/Users/excluding/\(userID)");
        var request = URLRequest(url:myUrl!);
        request.httpMethod = "GET";
        
        let config = URLSessionConfiguration.default;
        
        let session = URLSession(configuration: config, delegate: self, delegateQueue:OperationQueue.main);
        
        let task = session.dataTask(with: request, completionHandler: {
            data, response, error in
            
            if error != nil
            {
                print("URL Session error=\(error)");
                return;
            }
            
            do
            {
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary;
                
                if let parseJSON = json as? [String: Any]
                {
                    let status = parseJSON["status"] as? String;
                    
                    if(status == "success")
                    {
                        if let userData = parseJSON["user_data"] as? [Any]
                        {
                            self.addFriends = userData;
                        }
                    }
                    else if(status == "error")
                    {
                        self.addFriends = [];
                        
                        let errorMessage = parseJSON["message"];
                        print(errorMessage);
                    }
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    self.tableView.reloadData();
                })
            }
            catch let error as NSError {
                // Catch fires here, with an NSError being thrown from the JSONObjectWithData method
                print("A JSON parsing error occurred, here are the details:\n \(error)")
            }
            
            
        })
        
        task.resume();
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.addFriends.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:AddFriendsTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "addFriendsCell") as! AddFriendsTableViewCell;
        
        cell.selectionStyle = .none;
        
        let friendData = self.addFriends[(indexPath as NSIndexPath).row] as? [String:Any];
        
        let username = friendData?["first_name"] as? String;
        
        let userID = friendData?["user_id"] as? String;
        
        
        let userFriends = UserData.sharedInstance.getAllUserFriendData();
        
        for i in (0 ..< userFriends.count)
        {
            let currFriendData = self.addFriends[i] as? [String:Any];
            if let friendUserID = currFriendData?["user_id"] as? String
            {
                if(userID == friendUserID)
                {
                    if(currFriendData?["status"] as! String == "requested")
                    {
                        cell.loadItem(username!, friendStatus: .requested, friendUserID: Int(userID!)!);
                    }
                    else if(currFriendData?["status"] as! String == "accepted")
                    {
                        cell.loadItem(username!, friendStatus: .following, friendUserID: Int(userID!)!);
                    }
                    
                    return cell;
                }
                
            }
        }
        
        cell.loadItem(username!, friendStatus: .follow, friendUserID: Int(userID!)!);
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }

    @IBAction func backButtonTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil);
    }
}
