//
//  ViewController.swift
//  Wasup
//
//  Created by DomTom on 9/26/15.
//  Copyright © 2015 DomTom. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate, URLSessionDelegate, URLSessionTaskDelegate {

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userPasswordTextField: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad();
        // Do any additional setup after loading the view, typically from a nib.
        
        self.userPasswordTextField.delegate = self;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }
    
    func userSignIn()
    {
        
        let userName = userNameTextField?.text;
        let userPassword = userPasswordTextField?.text;
        
        // validate the info in the text fields
        if(userName!.isEmpty || userPassword!.isEmpty)
        {
            // TODO: display alert
            return;
        }
        
        // Send login info to server and wait for response
        let myUrl = URL(string: "http://ec2-54-68-92-237.us-west-2.compute.amazonaws.com/api/v1/UserLogin");
        var request = URLRequest(url:myUrl!);
        request.httpMethod = "POST";
        
        let postString = "username=\(userName!)&password=\(userPassword!)";
        
        request.httpBody = postString.data(using: String.Encoding.utf8);
        
        let config = URLSessionConfiguration.default;
        
        let session = URLSession(configuration: config, delegate: self, delegateQueue:OperationQueue.main);
        
        let task = session.dataTask(with: request, completionHandler: {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)");
                return;
            }
            
            do
            {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary;
                
                if let parseJSON = json {
                    let resultValue = parseJSON["status"] as? String;
                    let resultMSG = parseJSON["message"] as? String;
                    
                    print("Sign On Result: \(resultValue!) - \(resultMSG!)");
                    
                    if(resultValue == "Success")
                    {
                        // get the users ID out of the returned results
                        let resultUserID = parseJSON["user_id"] as? String;
                        
                        // Login is successfull!
                        UserDefaults.standard.set(true, forKey: "isUserLoggedIn");
                        UserDefaults.standard.set(Int(resultUserID!)!, forKey: "UserID");
                        UserDefaults.standard.synchronize();
                        
                        self.dismiss(animated: true, completion: nil);
                    }
                }
                
            }
            catch let error as NSError
            {
                // Catch fires here, with an NSError being thrown from the JSONObjectWithData method
                print("A JSON parsing error occurred, here are the details:\n \(error)");
            }
        }) 
        
        task.resume();
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        userSignIn();
        
        return true;
    }


    @IBAction func signOnButtonTapped(_ sender: UIButton)
    {
        self.userSignIn();
    }
}

