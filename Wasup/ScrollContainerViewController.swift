//
//  ScrollContainerViewController.swift
//  Wasup
//
//  Created by DomTom on 1/12/16.
//  Copyright © 2016 DomTom. All rights reserved.
//

import UIKit

class ScrollContainerViewController: UIViewController, UIScrollViewDelegate
{
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad();
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        
        // User Homepage
        let homepageVC = storyboard.instantiateViewController(withIdentifier: "UserHomepage");
        
        self.addChildViewController(homepageVC);
        self.scrollView.addSubview(homepageVC.view);
        homepageVC.didMove(toParentViewController: self);
        
        // User friends list
        let friendsListVC = storyboard.instantiateViewController(withIdentifier: "FriendsList");
        
        // offset the page by view width
        var friendsListFrame = friendsListVC.view.frame;
        friendsListFrame.origin.x = self.view.frame.size.width;
        friendsListVC.view.frame = friendsListFrame;
        
        self.addChildViewController(friendsListVC);
        self.scrollView.addSubview(friendsListVC.view);
        friendsListVC.didMove(toParentViewController: self);
        
        // TODO_THOMAS: hardcoded numbers in here HACKKKKKKKKKED
        // set the content size of the scroll view
        self.scrollView.contentSize = CGSize(width: self.view.frame.size.width * 2, height: self.view.frame.size.height - 68);
        
        self.scrollView.delegate = self;
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        //let width = scrollView.frame.size.width;
        //let pageIndex:Int = Int((scrollView.contentOffset.x + (0.5 * width)) / width);
    }

}
